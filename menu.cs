﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void menu_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void min_btn_login_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private int clickCounter_menu = 0;
        private void max_btn_login_Click(object sender, EventArgs e)
        {
            if (clickCounter_menu % 2 == 0)
            {
                this.WindowState = FormWindowState.Maximized;
                clickCounter_menu++;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                clickCounter_menu++;
            }
        }

        private void close_btn_login_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_calculator_Click(object sender, EventArgs e)
        {
            Calculator calculator = new Calculator();
            calculator.Show();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        int mouse_x_menu= 0;
        int mouse_y_menu = 0;
        bool mouse_down_menu;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_down_menu = true;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {

            if (mouse_down_menu)
            {
                mouse_x_menu = MousePosition.X - 200;
                mouse_y_menu = MousePosition.Y - 40;
                this.SetDesktopLocation(mouse_x_menu, mouse_y_menu);
            }

        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouse_down_menu = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Decryption_Encryption Decryption_Encryption = new Decryption_Encryption();
            Decryption_Encryption.Show();
        }

        private void pos_gusser_btn_Click(object sender, EventArgs e)
        {
            Position_Guesser Position_Guesser = new Position_Guesser();
            Position_Guesser.Show();
        }
    }
}
