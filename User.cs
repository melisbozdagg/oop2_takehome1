﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    struct UserPair
    {
        public string username { get; set; }
        public string hashedPassword { get; set; }
    }

    public sealed class User
    {

        private List<UserPair> userInfoList = new List<UserPair>();

        public void createUser(string _username, string _hashedPassword)
        {
            this.userInfoList.Add(new UserPair { username = _username, hashedPassword = _hashedPassword });
        }

        public bool check(string _username, string _hashedPassword)
        {
            foreach(var user_info in this.userInfoList)
            {
                if(user_info.username == _username && user_info.hashedPassword == _hashedPassword)
                {
                    return true;
                }
            }
            return false;
        }

        private static User instance = null;
        private static readonly object padlock = new object();

        User()
        {
        }

        public static User Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new User();
                    }
                    return instance;
                }
            }
        }
    }
}
