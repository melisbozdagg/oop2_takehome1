﻿
namespace lab1
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.LblWelcome = new System.Windows.Forms.Label();
            this.txtBoxUsername = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.BtnLogin = new System.Windows.Forms.Button();
            this.LblPassword = new System.Windows.Forms.Label();
            this.LblUsername = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.max_btn_login = new System.Windows.Forms.Button();
            this.min_btn_login = new System.Windows.Forms.Button();
            this.close_btn_login = new System.Windows.Forms.Button();
            this.panel_login = new System.Windows.Forms.Panel();
            this.BtnSignUp = new System.Windows.Forms.Button();
            this.password_lbl = new System.Windows.Forms.TextBox();
            this.panel_login.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblWelcome
            // 
            this.LblWelcome.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LblWelcome.AutoSize = true;
            this.LblWelcome.BackColor = System.Drawing.Color.RosyBrown;
            this.LblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblWelcome.ForeColor = System.Drawing.Color.Transparent;
            this.LblWelcome.Location = new System.Drawing.Point(155, 41);
            this.LblWelcome.Name = "LblWelcome";
            this.LblWelcome.Size = new System.Drawing.Size(180, 42);
            this.LblWelcome.TabIndex = 0;
            this.LblWelcome.Text = "Welcome";
            // 
            // txtBoxUsername
            // 
            this.txtBoxUsername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtBoxUsername.BackColor = System.Drawing.Color.RosyBrown;
            this.txtBoxUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.txtBoxUsername.ForeColor = System.Drawing.Color.White;
            this.txtBoxUsername.Location = new System.Drawing.Point(177, 146);
            this.txtBoxUsername.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtBoxUsername.Name = "txtBoxUsername";
            this.txtBoxUsername.ShortcutsEnabled = false;
            this.txtBoxUsername.Size = new System.Drawing.Size(175, 24);
            this.txtBoxUsername.TabIndex = 1;
            this.txtBoxUsername.MouseClick += new System.Windows.Forms.MouseEventHandler(this.username_MouseClick);
            this.txtBoxUsername.TextChanged += new System.EventHandler(this.txtBoxUsername_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // BtnLogin
            // 
            this.BtnLogin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnLogin.BackColor = System.Drawing.Color.RosyBrown;
            this.BtnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnLogin.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnLogin.Location = new System.Drawing.Point(164, 285);
            this.BtnLogin.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnLogin.Name = "BtnLogin";
            this.BtnLogin.Size = new System.Drawing.Size(175, 43);
            this.BtnLogin.TabIndex = 7;
            this.BtnLogin.Text = "LOGIN";
            this.BtnLogin.UseVisualStyleBackColor = false;
            this.BtnLogin.Click += new System.EventHandler(this.button1_Click);
            // 
            // LblPassword
            // 
            this.LblPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblPassword.AutoSize = true;
            this.LblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblPassword.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.LblPassword.Location = new System.Drawing.Point(4, 209);
            this.LblPassword.Name = "LblPassword";
            this.LblPassword.Size = new System.Drawing.Size(142, 29);
            this.LblPassword.TabIndex = 6;
            this.LblPassword.Text = "Password :";
            // 
            // LblUsername
            // 
            this.LblUsername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblUsername.AutoSize = true;
            this.LblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblUsername.ForeColor = System.Drawing.Color.Transparent;
            this.LblUsername.Location = new System.Drawing.Point(4, 146);
            this.LblUsername.Name = "LblUsername";
            this.LblUsername.Size = new System.Drawing.Size(146, 29);
            this.LblUsername.TabIndex = 5;
            this.LblUsername.Text = "Username :";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.Color.LavenderBlush;
            this.label5.Location = new System.Drawing.Point(413, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 31);
            this.label5.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.Snow;
            this.panel1.Location = new System.Drawing.Point(177, 179);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 1);
            this.panel1.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.Snow;
            this.panel2.Location = new System.Drawing.Point(177, 237);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(175, 1);
            this.panel2.TabIndex = 13;
            // 
            // max_btn_login
            // 
            this.max_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.max_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.max_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("max_btn_login.BackgroundImage")));
            this.max_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.max_btn_login.FlatAppearance.BorderSize = 0;
            this.max_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.max_btn_login.Location = new System.Drawing.Point(424, 8);
            this.max_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.max_btn_login.Name = "max_btn_login";
            this.max_btn_login.Size = new System.Drawing.Size(18, 26);
            this.max_btn_login.TabIndex = 14;
            this.max_btn_login.UseVisualStyleBackColor = false;
            this.max_btn_login.Click += new System.EventHandler(this.max_btn_Click);
            // 
            // min_btn_login
            // 
            this.min_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.min_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.min_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("min_btn_login.BackgroundImage")));
            this.min_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.min_btn_login.FlatAppearance.BorderSize = 0;
            this.min_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.min_btn_login.Location = new System.Drawing.Point(397, 8);
            this.min_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.min_btn_login.Name = "min_btn_login";
            this.min_btn_login.Size = new System.Drawing.Size(21, 24);
            this.min_btn_login.TabIndex = 15;
            this.min_btn_login.UseVisualStyleBackColor = false;
            this.min_btn_login.Click += new System.EventHandler(this.min_btn_Click);
            // 
            // close_btn_login
            // 
            this.close_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.close_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.close_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("close_btn_login.BackgroundImage")));
            this.close_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.close_btn_login.FlatAppearance.BorderSize = 0;
            this.close_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_btn_login.Location = new System.Drawing.Point(447, 10);
            this.close_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.close_btn_login.Name = "close_btn_login";
            this.close_btn_login.Size = new System.Drawing.Size(17, 22);
            this.close_btn_login.TabIndex = 16;
            this.close_btn_login.UseVisualStyleBackColor = false;
            this.close_btn_login.Click += new System.EventHandler(this.close_btn_Click);
            // 
            // panel_login
            // 
            this.panel_login.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_login.Controls.Add(this.min_btn_login);
            this.panel_login.Controls.Add(this.close_btn_login);
            this.panel_login.Controls.Add(this.max_btn_login);
            this.panel_login.Location = new System.Drawing.Point(-4, -5);
            this.panel_login.Name = "panel_login";
            this.panel_login.Size = new System.Drawing.Size(475, 40);
            this.panel_login.TabIndex = 17;
            this.panel_login.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_login_Paint);
            this.panel_login.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseDown);
            this.panel_login.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseMove);
            this.panel_login.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_login_MouseUp);
            // 
            // BtnSignUp
            // 
            this.BtnSignUp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnSignUp.BackColor = System.Drawing.Color.RosyBrown;
            this.BtnSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnSignUp.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnSignUp.Location = new System.Drawing.Point(164, 342);
            this.BtnSignUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnSignUp.Name = "BtnSignUp";
            this.BtnSignUp.Size = new System.Drawing.Size(175, 43);
            this.BtnSignUp.TabIndex = 18;
            this.BtnSignUp.Text = "SIGN UP";
            this.BtnSignUp.UseVisualStyleBackColor = false;
            this.BtnSignUp.Click += new System.EventHandler(this.BtnSignUp_Click);
            // 
            // password_lbl
            // 
            this.password_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.password_lbl.BackColor = System.Drawing.Color.RosyBrown;
            this.password_lbl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.password_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.password_lbl.ForeColor = System.Drawing.Color.White;
            this.password_lbl.Location = new System.Drawing.Point(177, 202);
            this.password_lbl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.password_lbl.Name = "password_lbl";
            this.password_lbl.PasswordChar = '*';
            this.password_lbl.Size = new System.Drawing.Size(175, 31);
            this.password_lbl.TabIndex = 2;
            this.password_lbl.MouseClick += new System.Windows.Forms.MouseEventHandler(this.password_MouseClick);
            this.password_lbl.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.password_KeyPress);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.ClientSize = new System.Drawing.Size(470, 422);
            this.Controls.Add(this.BtnSignUp);
            this.Controls.Add(this.panel_login);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BtnLogin);
            this.Controls.Add(this.txtBoxUsername);
            this.Controls.Add(this.LblPassword);
            this.Controls.Add(this.LblUsername);
            this.Controls.Add(this.password_lbl);
            this.Controls.Add(this.LblWelcome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_login.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Label LblWelcome;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.TextBox txtBoxUsername;
        private System.Windows.Forms.Button BtnLogin;
        private System.Windows.Forms.Label LblPassword;
        private System.Windows.Forms.Label LblUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button max_btn_login;
        private System.Windows.Forms.Button min_btn_login;
        private System.Windows.Forms.Button close_btn_login;
        private System.Windows.Forms.Panel panel_login;
        private System.Windows.Forms.Button BtnSignUp;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.TextBox password_lbl;
    }
}

