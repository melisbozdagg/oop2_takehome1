﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public enum pos
    {
        N, S, W, E, NW, SW, NE, SE
    }

    public class pos_guesser
    {
        private int x = 0;
        private int y = 0;

        private int xborder = 512;
        private int yborder = 512;
        public string coordinates()
        {
            return x.ToString() + "x" + y.ToString() + Environment.NewLine;
        }

        public void reset()
        {
            this.x = 0;
            this.y = 0;
            this.xborder = 512;
            this.yborder = 512;
        }
        public void Go(pos p)
        {
            switch (p)
            {
                case pos.N:
                    {
                        yf(1);
                        break;
                    }
                case pos.S:
                    {
                        yf(-1);
                        break;
                    }
                case pos.W:
                    {
                        xf(1);
                        break;
                    }
                case pos.E:
                    {
                        xf(-1);
                        break;
                    }
                case pos.NW:
                    {
                        yf(1);
                        xf(-1);
                        break;
                    }
                case pos.SW:
                    {
                        yf(-1);
                        xf(-1);
                        break;
                    }
                case pos.NE:
                    {
                        yf(1);
                        xf(1);
                        break;
                    }
                case pos.SE:
                    {
                        yf(-1);
                        xf(1);
                        break;
                    }
            }
            if(xborder == 2 || yborder == 2)
            {
                MessageBox.Show("You might made a mistake, please hit the reset button.", "Error", MessageBoxButtons.OK);
            }
        }
        private void yf(int v)
        {
            y += (yborder / 2) * v;
            yborder /= 2; yborder++;
        }
        private void xf(int v)
        {
            x += (xborder / 2) * v;
            xborder /= 2; xborder++;
        }
    }
}
