﻿
namespace lab1
{
    partial class SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SignUp));
            this.min_btn_signup = new System.Windows.Forms.Button();
            this.max_btn_signup = new System.Windows.Forms.Button();
            this.close_btn_signup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LblNewUsername = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbox_newUsername = new System.Windows.Forms.TextBox();
            this.password_lbl = new System.Windows.Forms.TextBox();
            this.BtnCreate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox_robot = new System.Windows.Forms.PictureBox();
            this.Lbl_robot = new System.Windows.Forms.Label();
            this.checkBox_robot = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbox_newpass_R = new System.Windows.Forms.TextBox();
            this.tbox_newpass = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LblRepeatPass = new System.Windows.Forms.Label();
            this.LblNewPassword = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tbox_email = new System.Windows.Forms.TextBox();
            this.LblSignUP = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_signup = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_robot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_signup.SuspendLayout();
            this.SuspendLayout();
            // 
            // min_btn_signup
            // 
            this.min_btn_signup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.min_btn_signup.BackColor = System.Drawing.Color.RosyBrown;
            this.min_btn_signup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("min_btn_signup.BackgroundImage")));
            this.min_btn_signup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.min_btn_signup.FlatAppearance.BorderSize = 0;
            this.min_btn_signup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.min_btn_signup.Location = new System.Drawing.Point(307, 10);
            this.min_btn_signup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.min_btn_signup.Name = "min_btn_signup";
            this.min_btn_signup.Size = new System.Drawing.Size(21, 24);
            this.min_btn_signup.TabIndex = 16;
            this.min_btn_signup.UseVisualStyleBackColor = false;
            this.min_btn_signup.Click += new System.EventHandler(this.min_btn_signup_Click);
            // 
            // max_btn_signup
            // 
            this.max_btn_signup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.max_btn_signup.BackColor = System.Drawing.Color.RosyBrown;
            this.max_btn_signup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("max_btn_signup.BackgroundImage")));
            this.max_btn_signup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.max_btn_signup.FlatAppearance.BorderSize = 0;
            this.max_btn_signup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.max_btn_signup.Location = new System.Drawing.Point(283, 9);
            this.max_btn_signup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.max_btn_signup.Name = "max_btn_signup";
            this.max_btn_signup.Size = new System.Drawing.Size(18, 26);
            this.max_btn_signup.TabIndex = 17;
            this.max_btn_signup.UseVisualStyleBackColor = false;
            this.max_btn_signup.Click += new System.EventHandler(this.max_btn_signup_Click);
            // 
            // close_btn_signup
            // 
            this.close_btn_signup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.close_btn_signup.BackColor = System.Drawing.Color.RosyBrown;
            this.close_btn_signup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("close_btn_signup.BackgroundImage")));
            this.close_btn_signup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.close_btn_signup.FlatAppearance.BorderSize = 0;
            this.close_btn_signup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_btn_signup.Location = new System.Drawing.Point(334, 11);
            this.close_btn_signup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.close_btn_signup.Name = "close_btn_signup";
            this.close_btn_signup.Size = new System.Drawing.Size(17, 22);
            this.close_btn_signup.TabIndex = 18;
            this.close_btn_signup.UseVisualStyleBackColor = false;
            this.close_btn_signup.Click += new System.EventHandler(this.close_btn_signup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-272, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 20;
            this.label1.Text = "label1";
            // 
            // LblNewUsername
            // 
            this.LblNewUsername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblNewUsername.AutoSize = true;
            this.LblNewUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblNewUsername.ForeColor = System.Drawing.Color.Transparent;
            this.LblNewUsername.Location = new System.Drawing.Point(12, 165);
            this.LblNewUsername.Name = "LblNewUsername";
            this.LblNewUsername.Size = new System.Drawing.Size(91, 20);
            this.LblNewUsername.TabIndex = 22;
            this.LblNewUsername.Text = "Username";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.Snow;
            this.panel1.Location = new System.Drawing.Point(168, 184);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(175, 1);
            this.panel1.TabIndex = 26;
            // 
            // tbox_newUsername
            // 
            this.tbox_newUsername.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbox_newUsername.BackColor = System.Drawing.Color.RosyBrown;
            this.tbox_newUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_newUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbox_newUsername.ForeColor = System.Drawing.Color.White;
            this.tbox_newUsername.Location = new System.Drawing.Point(168, 151);
            this.tbox_newUsername.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbox_newUsername.Name = "tbox_newUsername";
            this.tbox_newUsername.ShortcutsEnabled = false;
            this.tbox_newUsername.Size = new System.Drawing.Size(156, 24);
            this.tbox_newUsername.TabIndex = 24;
            // 
            // password_lbl
            // 
            this.password_lbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.password_lbl.BackColor = System.Drawing.Color.RosyBrown;
            this.password_lbl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.password_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.password_lbl.ForeColor = System.Drawing.Color.White;
            this.password_lbl.Location = new System.Drawing.Point(92, 324);
            this.password_lbl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.password_lbl.Name = "password_lbl";
            this.password_lbl.PasswordChar = '*';
            this.password_lbl.Size = new System.Drawing.Size(175, 31);
            this.password_lbl.TabIndex = 25;
            // 
            // BtnCreate
            // 
            this.BtnCreate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.BtnCreate.BackColor = System.Drawing.Color.RosyBrown;
            this.BtnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BtnCreate.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnCreate.Location = new System.Drawing.Point(71, 498);
            this.BtnCreate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnCreate.Name = "BtnCreate";
            this.BtnCreate.Size = new System.Drawing.Size(217, 36);
            this.BtnCreate.TabIndex = 31;
            this.BtnCreate.Text = "CREATE ACCOUNT";
            this.BtnCreate.UseVisualStyleBackColor = false;
            this.BtnCreate.Click += new System.EventHandler(this.BtnCreate_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.RosyBrown;
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.pictureBox_robot);
            this.panel2.Controls.Add(this.Lbl_robot);
            this.panel2.Controls.Add(this.checkBox_robot);
            this.panel2.Location = new System.Drawing.Point(25, 345);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(311, 127);
            this.panel2.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(44, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "I\'m not a robot";
            // 
            // pictureBox_robot
            // 
            this.pictureBox_robot.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_robot.Image")));
            this.pictureBox_robot.Location = new System.Drawing.Point(195, 22);
            this.pictureBox_robot.Name = "pictureBox_robot";
            this.pictureBox_robot.Size = new System.Drawing.Size(100, 80);
            this.pictureBox_robot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_robot.TabIndex = 24;
            this.pictureBox_robot.TabStop = false;
            // 
            // Lbl_robot
            // 
            this.Lbl_robot.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Lbl_robot.AutoSize = true;
            this.Lbl_robot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Lbl_robot.ForeColor = System.Drawing.Color.Transparent;
            this.Lbl_robot.Location = new System.Drawing.Point(46, 43);
            this.Lbl_robot.Name = "Lbl_robot";
            this.Lbl_robot.Size = new System.Drawing.Size(0, 20);
            this.Lbl_robot.TabIndex = 23;
            // 
            // checkBox_robot
            // 
            this.checkBox_robot.AutoSize = true;
            this.checkBox_robot.ForeColor = System.Drawing.SystemColors.Menu;
            this.checkBox_robot.Location = new System.Drawing.Point(23, 47);
            this.checkBox_robot.Name = "checkBox_robot";
            this.checkBox_robot.Size = new System.Drawing.Size(15, 14);
            this.checkBox_robot.TabIndex = 0;
            this.checkBox_robot.UseVisualStyleBackColor = true;
            this.checkBox_robot.CheckedChanged += new System.EventHandler(this.checkBox_robot_CheckedChanged);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(12, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 20);
            this.label3.TabIndex = 46;
            this.label3.Text = "E-mail";
            // 
            // tbox_newpass_R
            // 
            this.tbox_newpass_R.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbox_newpass_R.BackColor = System.Drawing.Color.RosyBrown;
            this.tbox_newpass_R.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_newpass_R.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbox_newpass_R.ForeColor = System.Drawing.Color.White;
            this.tbox_newpass_R.Location = new System.Drawing.Point(177, 285);
            this.tbox_newpass_R.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbox_newpass_R.Name = "tbox_newpass_R";
            this.tbox_newpass_R.PasswordChar = '*';
            this.tbox_newpass_R.Size = new System.Drawing.Size(175, 31);
            this.tbox_newpass_R.TabIndex = 45;
            // 
            // tbox_newpass
            // 
            this.tbox_newpass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbox_newpass.BackColor = System.Drawing.Color.RosyBrown;
            this.tbox_newpass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_newpass.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbox_newpass.ForeColor = System.Drawing.Color.White;
            this.tbox_newpass.Location = new System.Drawing.Point(177, 235);
            this.tbox_newpass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbox_newpass.Name = "tbox_newpass";
            this.tbox_newpass.PasswordChar = '*';
            this.tbox_newpass.Size = new System.Drawing.Size(175, 31);
            this.tbox_newpass.TabIndex = 44;
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel4.BackColor = System.Drawing.Color.Snow;
            this.panel4.Location = new System.Drawing.Point(177, 320);
            this.panel4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(175, 1);
            this.panel4.TabIndex = 43;
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.Snow;
            this.panel3.Location = new System.Drawing.Point(177, 270);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(175, 1);
            this.panel3.TabIndex = 42;
            // 
            // LblRepeatPass
            // 
            this.LblRepeatPass.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblRepeatPass.AutoSize = true;
            this.LblRepeatPass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblRepeatPass.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.LblRepeatPass.Location = new System.Drawing.Point(9, 301);
            this.LblRepeatPass.Name = "LblRepeatPass";
            this.LblRepeatPass.Size = new System.Drawing.Size(150, 20);
            this.LblRepeatPass.TabIndex = 41;
            this.LblRepeatPass.Text = "Repeat Password";
            // 
            // LblNewPassword
            // 
            this.LblNewPassword.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblNewPassword.AutoSize = true;
            this.LblNewPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblNewPassword.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            this.LblNewPassword.Location = new System.Drawing.Point(12, 251);
            this.LblNewPassword.Name = "LblNewPassword";
            this.LblNewPassword.Size = new System.Drawing.Size(91, 20);
            this.LblNewPassword.TabIndex = 40;
            this.LblNewPassword.Text = "Password ";
            // 
            // panel5
            // 
            this.panel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel5.BackColor = System.Drawing.Color.Snow;
            this.panel5.Location = new System.Drawing.Point(168, 227);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(175, 1);
            this.panel5.TabIndex = 48;
            // 
            // tbox_email
            // 
            this.tbox_email.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tbox_email.BackColor = System.Drawing.Color.RosyBrown;
            this.tbox_email.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbox_email.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tbox_email.ForeColor = System.Drawing.Color.White;
            this.tbox_email.Location = new System.Drawing.Point(168, 194);
            this.tbox_email.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbox_email.Name = "tbox_email";
            this.tbox_email.ShortcutsEnabled = false;
            this.tbox_email.Size = new System.Drawing.Size(156, 24);
            this.tbox_email.TabIndex = 47;
            // 
            // LblSignUP
            // 
            this.LblSignUP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LblSignUP.AutoSize = true;
            this.LblSignUP.BackColor = System.Drawing.Color.RosyBrown;
            this.LblSignUP.Font = new System.Drawing.Font("Trebuchet MS", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.LblSignUP.ForeColor = System.Drawing.Color.Transparent;
            this.LblSignUP.Location = new System.Drawing.Point(92, 64);
            this.LblSignUP.Name = "LblSignUP";
            this.LblSignUP.Size = new System.Drawing.Size(157, 46);
            this.LblSignUP.TabIndex = 49;
            this.LblSignUP.Text = "SIGN UP";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(35, 64);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(51, 46);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 50;
            this.pictureBox1.TabStop = false;
            // 
            // panel_signup
            // 
            this.panel_signup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_signup.Controls.Add(this.button1);
            this.panel_signup.Controls.Add(this.button2);
            this.panel_signup.Controls.Add(this.button3);
            this.panel_signup.Controls.Add(this.min_btn_signup);
            this.panel_signup.Controls.Add(this.max_btn_signup);
            this.panel_signup.Controls.Add(this.close_btn_signup);
            this.panel_signup.Location = new System.Drawing.Point(1, 1);
            this.panel_signup.Name = "panel_signup";
            this.panel_signup.Size = new System.Drawing.Size(366, 49);
            this.panel_signup.TabIndex = 51;
            this.panel_signup.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_signup_MouseDown);
            this.panel_signup.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_signup_MouseMove);
            this.panel_signup.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_signup_MouseUp);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.RosyBrown;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(563, 8);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(21, 24);
            this.button1.TabIndex = 15;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.BackColor = System.Drawing.Color.RosyBrown;
            this.button2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button2.BackgroundImage")));
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(613, 10);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(17, 22);
            this.button2.TabIndex = 16;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.BackColor = System.Drawing.Color.RosyBrown;
            this.button3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button3.BackgroundImage")));
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(590, 8);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(18, 26);
            this.button3.TabIndex = 14;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.ClientSize = new System.Drawing.Size(365, 567);
            this.Controls.Add(this.panel_signup);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.LblSignUP);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.tbox_email);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbox_newpass_R);
            this.Controls.Add(this.tbox_newpass);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.LblRepeatPass);
            this.Controls.Add(this.LblNewPassword);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.BtnCreate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbox_newUsername);
            this.Controls.Add(this.password_lbl);
            this.Controls.Add(this.LblNewUsername);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Coral;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignUp";
            this.Load += new System.EventHandler(this.SignUp_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_robot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_signup.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button min_btn_signup;
        private System.Windows.Forms.Button max_btn_signup;
        private System.Windows.Forms.Button close_btn_signup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LblNewUsername;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox tbox_newUsername;
        public System.Windows.Forms.TextBox password_lbl;
        private System.Windows.Forms.Button BtnCreate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox_robot;
        private System.Windows.Forms.Label Lbl_robot;
        private System.Windows.Forms.CheckBox checkBox_robot;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox tbox_newpass_R;
        public System.Windows.Forms.TextBox tbox_newpass;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label LblRepeatPass;
        private System.Windows.Forms.Label LblNewPassword;
        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.TextBox tbox_email;
        public System.Windows.Forms.Label LblSignUP;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel_signup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}