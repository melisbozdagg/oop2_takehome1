﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public class En_Decryption
    {

        public static string Ceasar(string alphabet, string message, int rot)
        {
           
            string res = "";
            for (int i = 0; i < message.Length; i++)
            {
                int flag = 0;
                for (int j = 0; j < alphabet.Length; j++)
                {
                    if (message[i] == alphabet[j] || message[i] == ' ')
                    {
                        flag = 1;
                    }
                }
                if (flag == 1)
                {
                    if(message[i] == ' ')
                    {
                        res += ' ';
                    }
                    else
                     res += alphabet[((message[i] + rot) + (Math.Abs(rot) * alphabet.Length)) % alphabet.Length];
                }
                else
                {
                    MessageBox.Show("A letter in the message content is not in the alphabet!", "Error", MessageBoxButtons.OK);
                    return "";
                }
            }
            return res;
        }
        public static string Vigenère(string alphabet, string message, string key, bool enc)
        {
            int st = 0, sign = 1;
       
            for(int i = 0; i < message.Length; i++)
            {
                if (!alphabet.Contains(message[i]) && message[i] != ' ')
                {
                    MessageBox.Show("A letter in the message content is not in the alphabet!");
                    return "";
                }
            }
            if (!enc) { st = alphabet.Length; sign = -1; }
            if (IsNumber(key))
            {
                MessageBox.Show("Key must be string!");
                return "";
            }
            string res = "";
            int size = message.Length, keys = key.Length;
            for (int i = 0; i <= size; i++)
            {
                if (key.Length < i )
                {
                    key += key[(i - 1) % keys];
                }
        
            }
            //----
            for (int i = 0; i < size; i++)
            {
                if (message[i] == ' ')
                {
                    res += ' ';
                }
                else
                {
                    res += alphabet[(CharIndex(alphabet, message[i]) + sign * CharIndex(alphabet, key[i]) + st) % alphabet.Length];
                }
                   
                
            }

            return res;
        }

        private static bool IsNumber(string key)
        {
            double oReturn = 0;
            return double.TryParse(key, out oReturn);
        }

        private static int CharIndex(string str, char c)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    return i;
            }
            return -1;
        }
    }
}
