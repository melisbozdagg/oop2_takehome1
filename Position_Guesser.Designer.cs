﻿
namespace lab1
{
    partial class Position_Guesser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_nw = new System.Windows.Forms.Button();
            this.btn_n = new System.Windows.Forms.Button();
            this.btn_ne = new System.Windows.Forms.Button();
            this.btn_e = new System.Windows.Forms.Button();
            this.btn_w = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.btn_sw = new System.Windows.Forms.Button();
            this.btn_s = new System.Windows.Forms.Button();
            this.btn_se = new System.Windows.Forms.Button();
            this.label_results = new System.Windows.Forms.Label();
            this.lbl_guesses = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_nw
            // 
            this.btn_nw.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_nw.Location = new System.Drawing.Point(333, 142);
            this.btn_nw.Name = "btn_nw";
            this.btn_nw.Size = new System.Drawing.Size(67, 64);
            this.btn_nw.TabIndex = 0;
            this.btn_nw.Text = "NW";
            this.btn_nw.UseVisualStyleBackColor = true;
            this.btn_nw.Click += new System.EventHandler(this.btn_nw_Click);
            // 
            // btn_n
            // 
            this.btn_n.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_n.Location = new System.Drawing.Point(406, 142);
            this.btn_n.Name = "btn_n";
            this.btn_n.Size = new System.Drawing.Size(67, 64);
            this.btn_n.TabIndex = 1;
            this.btn_n.Text = "N";
            this.btn_n.UseVisualStyleBackColor = true;
            this.btn_n.Click += new System.EventHandler(this.btn_n_Click);
            // 
            // btn_ne
            // 
            this.btn_ne.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ne.Location = new System.Drawing.Point(479, 142);
            this.btn_ne.Name = "btn_ne";
            this.btn_ne.Size = new System.Drawing.Size(67, 64);
            this.btn_ne.TabIndex = 2;
            this.btn_ne.Text = "NE";
            this.btn_ne.UseVisualStyleBackColor = true;
            this.btn_ne.Click += new System.EventHandler(this.btn_ne_Click);
            // 
            // btn_e
            // 
            this.btn_e.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_e.Location = new System.Drawing.Point(479, 212);
            this.btn_e.Name = "btn_e";
            this.btn_e.Size = new System.Drawing.Size(67, 64);
            this.btn_e.TabIndex = 3;
            this.btn_e.Text = "E";
            this.btn_e.UseVisualStyleBackColor = true;
            this.btn_e.Click += new System.EventHandler(this.btn_e_Click);
            // 
            // btn_w
            // 
            this.btn_w.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_w.Location = new System.Drawing.Point(333, 212);
            this.btn_w.Name = "btn_w";
            this.btn_w.Size = new System.Drawing.Size(67, 64);
            this.btn_w.TabIndex = 4;
            this.btn_w.Text = "W";
            this.btn_w.UseVisualStyleBackColor = true;
            this.btn_w.Click += new System.EventHandler(this.btn_w_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reset.Location = new System.Drawing.Point(406, 212);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(67, 64);
            this.btn_reset.TabIndex = 5;
            this.btn_reset.Text = "RES";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // btn_sw
            // 
            this.btn_sw.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sw.Location = new System.Drawing.Point(333, 282);
            this.btn_sw.Name = "btn_sw";
            this.btn_sw.Size = new System.Drawing.Size(67, 64);
            this.btn_sw.TabIndex = 6;
            this.btn_sw.Text = "SW";
            this.btn_sw.UseVisualStyleBackColor = true;
            this.btn_sw.Click += new System.EventHandler(this.btn_sw_Click);
            // 
            // btn_s
            // 
            this.btn_s.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_s.Location = new System.Drawing.Point(406, 282);
            this.btn_s.Name = "btn_s";
            this.btn_s.Size = new System.Drawing.Size(67, 64);
            this.btn_s.TabIndex = 7;
            this.btn_s.Text = "S";
            this.btn_s.UseVisualStyleBackColor = true;
            this.btn_s.Click += new System.EventHandler(this.btn_s_Click);
            // 
            // btn_se
            // 
            this.btn_se.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_se.Location = new System.Drawing.Point(479, 282);
            this.btn_se.Name = "btn_se";
            this.btn_se.Size = new System.Drawing.Size(67, 64);
            this.btn_se.TabIndex = 8;
            this.btn_se.Text = "SE";
            this.btn_se.UseVisualStyleBackColor = true;
            this.btn_se.Click += new System.EventHandler(this.btn_se_Click);
            // 
            // label_results
            // 
            this.label_results.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_results.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label_results.ForeColor = System.Drawing.Color.Transparent;
            this.label_results.Location = new System.Drawing.Point(40, 102);
            this.label_results.Name = "label_results";
            this.label_results.Size = new System.Drawing.Size(273, 413);
            this.label_results.TabIndex = 9;
            this.label_results.Text = "RESULTS";
            // 
            // lbl_guesses
            // 
            this.lbl_guesses.AutoSize = true;
            this.lbl_guesses.BackColor = System.Drawing.Color.Black;
            this.lbl_guesses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_guesses.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_guesses.Location = new System.Drawing.Point(40, 31);
            this.lbl_guesses.Name = "lbl_guesses";
            this.lbl_guesses.Size = new System.Drawing.Size(116, 25);
            this.lbl_guesses.TabIndex = 10;
            this.lbl_guesses.Text = "GUESSES";
            // 
            // Position_Guesser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(575, 564);
            this.Controls.Add(this.lbl_guesses);
            this.Controls.Add(this.label_results);
            this.Controls.Add(this.btn_se);
            this.Controls.Add(this.btn_s);
            this.Controls.Add(this.btn_sw);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_w);
            this.Controls.Add(this.btn_e);
            this.Controls.Add(this.btn_ne);
            this.Controls.Add(this.btn_n);
            this.Controls.Add(this.btn_nw);
            this.Name = "Position_Guesser";
            this.Text = "Position_Guesser";
            this.Load += new System.EventHandler(this.Position_Guesser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_nw;
        private System.Windows.Forms.Button btn_n;
        private System.Windows.Forms.Button btn_ne;
        private System.Windows.Forms.Button btn_e;
        private System.Windows.Forms.Button btn_w;
        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.Button btn_sw;
        private System.Windows.Forms.Button btn_s;
        private System.Windows.Forms.Button btn_se;
        private System.Windows.Forms.Label label_results;
        private System.Windows.Forms.Label lbl_guesses;
    }
}