﻿
namespace lab1
{
    partial class Decryption_Encryption
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_options = new System.Windows.Forms.GroupBox();
            this.radio_btn_Vigenèrecipher = new System.Windows.Forms.RadioButton();
            this.radiobtn_CeaserCipher = new System.Windows.Forms.RadioButton();
            this.lbl_dec = new System.Windows.Forms.Label();
            this.groupBox_option2 = new System.Windows.Forms.GroupBox();
            this.radioBtn_Decry = new System.Windows.Forms.RadioButton();
            this.radioBtn_Enc = new System.Windows.Forms.RadioButton();
            this.textBox_key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbl_result = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_message = new System.Windows.Forms.TextBox();
            this.textBox_alphabet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox_options.SuspendLayout();
            this.groupBox_option2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_options
            // 
            this.groupBox_options.Controls.Add(this.radio_btn_Vigenèrecipher);
            this.groupBox_options.Controls.Add(this.radiobtn_CeaserCipher);
            this.groupBox_options.Location = new System.Drawing.Point(29, 156);
            this.groupBox_options.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox_options.Name = "groupBox_options";
            this.groupBox_options.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox_options.Size = new System.Drawing.Size(251, 100);
            this.groupBox_options.TabIndex = 1;
            this.groupBox_options.TabStop = false;
            // 
            // radio_btn_Vigenèrecipher
            // 
            this.radio_btn_Vigenèrecipher.AutoSize = true;
            this.radio_btn_Vigenèrecipher.Location = new System.Drawing.Point(21, 63);
            this.radio_btn_Vigenèrecipher.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radio_btn_Vigenèrecipher.Name = "radio_btn_Vigenèrecipher";
            this.radio_btn_Vigenèrecipher.Size = new System.Drawing.Size(129, 21);
            this.radio_btn_Vigenèrecipher.TabIndex = 1;
            this.radio_btn_Vigenèrecipher.Text = "Vigenère cipher";
            this.radio_btn_Vigenèrecipher.UseVisualStyleBackColor = true;
            this.radio_btn_Vigenèrecipher.CheckedChanged += new System.EventHandler(this.radio_btn_Vigenèrecipher_CheckedChanged_1);
            // 
            // radiobtn_CeaserCipher
            // 
            this.radiobtn_CeaserCipher.AutoSize = true;
            this.radiobtn_CeaserCipher.Location = new System.Drawing.Point(21, 30);
            this.radiobtn_CeaserCipher.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radiobtn_CeaserCipher.Name = "radiobtn_CeaserCipher";
            this.radiobtn_CeaserCipher.Size = new System.Drawing.Size(119, 21);
            this.radiobtn_CeaserCipher.TabIndex = 0;
            this.radiobtn_CeaserCipher.Text = "Ceaser Cipher";
            this.radiobtn_CeaserCipher.UseVisualStyleBackColor = true;
            this.radiobtn_CeaserCipher.CheckedChanged += new System.EventHandler(this.radiobtn_CeaserCipher_CheckedChanged_1);
            // 
            // lbl_dec
            // 
            this.lbl_dec.AutoSize = true;
            this.lbl_dec.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold);
            this.lbl_dec.Location = new System.Drawing.Point(352, 106);
            this.lbl_dec.Name = "lbl_dec";
            this.lbl_dec.Size = new System.Drawing.Size(188, 29);
            this.lbl_dec.TabIndex = 2;
            this.lbl_dec.Text = "Enter Message";
            // 
            // groupBox_option2
            // 
            this.groupBox_option2.Controls.Add(this.radioBtn_Decry);
            this.groupBox_option2.Controls.Add(this.radioBtn_Enc);
            this.groupBox_option2.Location = new System.Drawing.Point(29, 52);
            this.groupBox_option2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox_option2.Name = "groupBox_option2";
            this.groupBox_option2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox_option2.Size = new System.Drawing.Size(252, 100);
            this.groupBox_option2.TabIndex = 3;
            this.groupBox_option2.TabStop = false;
            // 
            // radioBtn_Decry
            // 
            this.radioBtn_Decry.AutoSize = true;
            this.radioBtn_Decry.Location = new System.Drawing.Point(29, 63);
            this.radioBtn_Decry.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioBtn_Decry.Name = "radioBtn_Decry";
            this.radioBtn_Decry.Size = new System.Drawing.Size(97, 21);
            this.radioBtn_Decry.TabIndex = 3;
            this.radioBtn_Decry.TabStop = true;
            this.radioBtn_Decry.Text = "Decryption";
            this.radioBtn_Decry.UseVisualStyleBackColor = true;
            this.radioBtn_Decry.CheckedChanged += new System.EventHandler(this.radioBtn_Decry_CheckedChanged);
            // 
            // radioBtn_Enc
            // 
            this.radioBtn_Enc.AutoSize = true;
            this.radioBtn_Enc.Location = new System.Drawing.Point(29, 30);
            this.radioBtn_Enc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.radioBtn_Enc.Name = "radioBtn_Enc";
            this.radioBtn_Enc.Size = new System.Drawing.Size(96, 21);
            this.radioBtn_Enc.TabIndex = 2;
            this.radioBtn_Enc.Text = "Encryption";
            this.radioBtn_Enc.UseVisualStyleBackColor = true;
            this.radioBtn_Enc.CheckedChanged += new System.EventHandler(this.radioBtn_Enc_CheckedChanged_1);
            // 
            // textBox_key
            // 
            this.textBox_key.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_key.Location = new System.Drawing.Point(29, 309);
            this.textBox_key.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_key.Name = "textBox_key";
            this.textBox_key.Size = new System.Drawing.Size(252, 24);
            this.textBox_key.TabIndex = 5;
            this.textBox_key.TextChanged += new System.EventHandler(this.textBox_key_TextChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(27, 277);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Enter Key / ROT ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Lavender;
            this.label2.Location = new System.Drawing.Point(429, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 17);
            this.label2.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox1.Controls.Add(this.lbl_result);
            this.groupBox1.Location = new System.Drawing.Point(357, 277);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(417, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // lbl_result
            // 
            this.lbl_result.Location = new System.Drawing.Point(0, 0);
            this.lbl_result.Name = "lbl_result";
            this.lbl_result.Size = new System.Drawing.Size(417, 100);
            this.lbl_result.TabIndex = 0;
            this.lbl_result.Text = "Result";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.MediumPurple;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(8, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(332, 32);
            this.label4.TabIndex = 21;
            this.label4.Text = "Decryption - Encryption";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(352, 246);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 29);
            this.label5.TabIndex = 22;
            this.label5.Text = "Result";
            // 
            // textBox_message
            // 
            this.textBox_message.Location = new System.Drawing.Point(357, 137);
            this.textBox_message.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_message.Multiline = true;
            this.textBox_message.Name = "textBox_message";
            this.textBox_message.Size = new System.Drawing.Size(416, 102);
            this.textBox_message.TabIndex = 23;
            this.textBox_message.TextChanged += new System.EventHandler(this.textBox_message_TextChanged_1);
            // 
            // textBox_alphabet
            // 
            this.textBox_alphabet.Location = new System.Drawing.Point(357, 50);
            this.textBox_alphabet.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox_alphabet.Multiline = true;
            this.textBox_alphabet.Name = "textBox_alphabet";
            this.textBox_alphabet.Size = new System.Drawing.Size(416, 38);
            this.textBox_alphabet.TabIndex = 24;
            this.textBox_alphabet.TextChanged += new System.EventHandler(this.textBox_alphabet_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(352, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(185, 29);
            this.label3.TabIndex = 25;
            this.label3.Text = "Enter Alphabet";
            // 
            // Decryption_Encryption
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumPurple;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_alphabet);
            this.Controls.Add(this.textBox_message);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_key);
            this.Controls.Add(this.groupBox_option2);
            this.Controls.Add(this.lbl_dec);
            this.Controls.Add(this.groupBox_options);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Decryption_Encryption";
            this.Text = "Decryption_Encryption";
            this.Load += new System.EventHandler(this.Decryption_Encryption_Load);
            this.groupBox_options.ResumeLayout(false);
            this.groupBox_options.PerformLayout();
            this.groupBox_option2.ResumeLayout(false);
            this.groupBox_option2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox_options;
        private System.Windows.Forms.RadioButton radio_btn_Vigenèrecipher;
        private System.Windows.Forms.RadioButton radiobtn_CeaserCipher;
        private System.Windows.Forms.Label lbl_dec;
        private System.Windows.Forms.GroupBox groupBox_option2;
        private System.Windows.Forms.RadioButton radioBtn_Decry;
        private System.Windows.Forms.RadioButton radioBtn_Enc;
        private System.Windows.Forms.TextBox textBox_key;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_message;
        private System.Windows.Forms.Label lbl_result;
        private System.Windows.Forms.TextBox textBox_alphabet;
        private System.Windows.Forms.Label label3;
    }
}