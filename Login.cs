﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using static lab1.Functions;


namespace lab1
{
    public partial class FormLogin : Form
    {
        User user = User.Instance;

        Timer timer = new Timer();

        public FormLogin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            user.createUser("Melis",MD5Hash("1055"));
            user.createUser("Mislina", MD5Hash("1055"));
            user.createUser("Talha", MD5Hash("1055"));
            user.createUser("Hüseyin", MD5Hash("1055"));

        }

        private void username_MouseClick(object sender, MouseEventArgs e)
        {
            txtBoxUsername.Text = string.Empty;
        }

        private void password_MouseClick(object sender, MouseEventArgs e)
        {
            password_lbl.Text = string.Empty;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer.Stop();
            menu menu = new menu();
            this.Hide();
            menu.Show();
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtBoxUsername.Text == "" || password_lbl.Text == "")
            {
                MessageBox.Show("Please Enter Username or Password!");
                return;
            }

            if (user.check(txtBoxUsername.Text, MD5Hash(password_lbl.Text)))
            {
                LblWelcome.Text = "Success!";
                LblWelcome.ForeColor = System.Drawing.Color.Green;
                timer.Tick += new EventHandler(timer1_Tick);
                timer.Interval = 3000;
                timer.Start();
                BtnLogin.Enabled = false;
            }
            else
            {
                LblWelcome.Text = "Error!";
                LblWelcome.ForeColor = System.Drawing.Color.Red;

            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                button1_Click(sender, e);
            }
        }


        private void min_btn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void close_btn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private int clickCounter = 0;
        private void max_btn_Click(object sender, EventArgs e)
        {
            if (clickCounter % 2 == 0)
            {
                this.WindowState = FormWindowState.Maximized;
                clickCounter++;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                clickCounter++;
            }
        }

        int mouse_x = 0;
        int mouse_y = 0;
        bool mouse_down;

        private void panel_login_MouseUp(object sender, MouseEventArgs e)
        {
            mouse_down = false;
        }

        private void panel_login_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouse_down)
            {
                mouse_x = MousePosition.X - 200;
                mouse_y = MousePosition.Y - 40;
                this.SetDesktopLocation(mouse_x, mouse_y);
            }

        }

        private void panel_login_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_down = true;
        }

        

        private void panel_login_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtBoxUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnSignUp_Click(object sender, EventArgs e)
        {
            SignUp fr = new SignUp();
            fr.Show();
            // this.Hide();



        }
    }
}