﻿
namespace lab1
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Calculator));
            this.txtInput1 = new System.Windows.Forms.RichTextBox();
            this.txtInput2 = new System.Windows.Forms.RichTextBox();
            this.buttPlus = new System.Windows.Forms.Button();
            this.buttMinus = new System.Windows.Forms.Button();
            this.buttMultip = new System.Windows.Forms.Button();
            this.buttDivision = new System.Windows.Forms.Button();
            this.labelRes = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.max_btn_cal = new System.Windows.Forms.Button();
            this.close_btn_cal = new System.Windows.Forms.Button();
            this.min_btn_cal = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtInput1
            // 
            this.txtInput1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInput1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInput1.Location = new System.Drawing.Point(37, 84);
            this.txtInput1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtInput1.Name = "txtInput1";
            this.txtInput1.Size = new System.Drawing.Size(121, 40);
            this.txtInput1.TabIndex = 2;
            this.txtInput1.Text = "";
            // 
            // txtInput2
            // 
            this.txtInput2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtInput2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInput2.Location = new System.Drawing.Point(37, 140);
            this.txtInput2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtInput2.Name = "txtInput2";
            this.txtInput2.Size = new System.Drawing.Size(121, 40);
            this.txtInput2.TabIndex = 3;
            this.txtInput2.Text = "";
            // 
            // buttPlus
            // 
            this.buttPlus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttPlus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttPlus.Location = new System.Drawing.Point(185, 31);
            this.buttPlus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttPlus.Name = "buttPlus";
            this.buttPlus.Size = new System.Drawing.Size(40, 39);
            this.buttPlus.TabIndex = 4;
            this.buttPlus.Text = "+";
            this.buttPlus.UseVisualStyleBackColor = true;
            this.buttPlus.Click += new System.EventHandler(this.buttPlus_Click);
            this.buttPlus.MouseLeave += new System.EventHandler(this.buttMultip_MouseLeave);
            this.buttPlus.MouseHover += new System.EventHandler(this.buttMultip_MouseHover);
            // 
            // buttMinus
            // 
            this.buttMinus.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttMinus.Location = new System.Drawing.Point(185, 141);
            this.buttMinus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttMinus.Name = "buttMinus";
            this.buttMinus.Size = new System.Drawing.Size(40, 39);
            this.buttMinus.TabIndex = 5;
            this.buttMinus.Text = "-";
            this.buttMinus.UseVisualStyleBackColor = true;
            this.buttMinus.Click += new System.EventHandler(this.buttMinus_Click);
            this.buttMinus.MouseLeave += new System.EventHandler(this.buttMultip_MouseLeave);
            this.buttMinus.MouseHover += new System.EventHandler(this.buttMultip_MouseHover);
            // 
            // buttMultip
            // 
            this.buttMultip.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttMultip.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttMultip.Location = new System.Drawing.Point(185, 85);
            this.buttMultip.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttMultip.Name = "buttMultip";
            this.buttMultip.Size = new System.Drawing.Size(40, 39);
            this.buttMultip.TabIndex = 6;
            this.buttMultip.Text = "*";
            this.buttMultip.UseVisualStyleBackColor = true;
            this.buttMultip.Click += new System.EventHandler(this.buttMultip_Click);
            this.buttMultip.MouseLeave += new System.EventHandler(this.buttMultip_MouseLeave);
            this.buttMultip.MouseHover += new System.EventHandler(this.buttMultip_MouseHover);
            // 
            // buttDivision
            // 
            this.buttDivision.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.buttDivision.Location = new System.Drawing.Point(185, 198);
            this.buttDivision.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttDivision.Name = "buttDivision";
            this.buttDivision.Size = new System.Drawing.Size(40, 39);
            this.buttDivision.TabIndex = 7;
            this.buttDivision.Text = "/";
            this.buttDivision.UseVisualStyleBackColor = true;
            this.buttDivision.Click += new System.EventHandler(this.buttDivision_Click);
            this.buttDivision.MouseLeave += new System.EventHandler(this.buttMultip_MouseLeave);
            this.buttDivision.MouseHover += new System.EventHandler(this.buttMultip_MouseHover);
            // 
            // labelRes
            // 
            this.labelRes.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelRes.AutoSize = true;
            this.labelRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRes.Location = new System.Drawing.Point(253, 117);
            this.labelRes.Name = "labelRes";
            this.labelRes.Size = new System.Drawing.Size(2, 31);
            this.labelRes.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.max_btn_cal);
            this.panel1.Controls.Add(this.close_btn_cal);
            this.panel1.Controls.Add(this.min_btn_cal);
            this.panel1.Location = new System.Drawing.Point(-3, -11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(492, 37);
            this.panel1.TabIndex = 10;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // max_btn_cal
            // 
            this.max_btn_cal.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.max_btn_cal.BackColor = System.Drawing.Color.White;
            this.max_btn_cal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("max_btn_cal.BackgroundImage")));
            this.max_btn_cal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.max_btn_cal.FlatAppearance.BorderSize = 0;
            this.max_btn_cal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.max_btn_cal.Location = new System.Drawing.Point(426, 11);
            this.max_btn_cal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.max_btn_cal.Name = "max_btn_cal";
            this.max_btn_cal.Size = new System.Drawing.Size(21, 23);
            this.max_btn_cal.TabIndex = 15;
            this.max_btn_cal.UseVisualStyleBackColor = false;
            this.max_btn_cal.Click += new System.EventHandler(this.max_btn_cal_Click);
            // 
            // close_btn_cal
            // 
            this.close_btn_cal.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.close_btn_cal.BackColor = System.Drawing.Color.White;
            this.close_btn_cal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("close_btn_cal.BackgroundImage")));
            this.close_btn_cal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.close_btn_cal.FlatAppearance.BorderSize = 0;
            this.close_btn_cal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_btn_cal.Location = new System.Drawing.Point(453, 11);
            this.close_btn_cal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.close_btn_cal.Name = "close_btn_cal";
            this.close_btn_cal.Size = new System.Drawing.Size(19, 23);
            this.close_btn_cal.TabIndex = 17;
            this.close_btn_cal.UseVisualStyleBackColor = false;
            this.close_btn_cal.Click += new System.EventHandler(this.close_btn_cal_Click);
            // 
            // min_btn_cal
            // 
            this.min_btn_cal.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.min_btn_cal.BackColor = System.Drawing.Color.White;
            this.min_btn_cal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("min_btn_cal.BackgroundImage")));
            this.min_btn_cal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.min_btn_cal.FlatAppearance.BorderSize = 0;
            this.min_btn_cal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.min_btn_cal.Location = new System.Drawing.Point(400, 11);
            this.min_btn_cal.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.min_btn_cal.Name = "min_btn_cal";
            this.min_btn_cal.Size = new System.Drawing.Size(20, 23);
            this.min_btn_cal.TabIndex = 16;
            this.min_btn_cal.UseVisualStyleBackColor = false;
            this.min_btn_cal.Click += new System.EventHandler(this.min_btn_cal_Click);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(481, 278);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.labelRes);
            this.Controls.Add(this.buttDivision);
            this.Controls.Add(this.buttMultip);
            this.Controls.Add(this.buttMinus);
            this.Controls.Add(this.buttPlus);
            this.Controls.Add(this.txtInput2);
            this.Controls.Add(this.txtInput1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.Calculator_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtInput1;
        private System.Windows.Forms.RichTextBox txtInput2;
        private System.Windows.Forms.Button buttPlus;
        private System.Windows.Forms.Button buttMinus;
        private System.Windows.Forms.Button buttMultip;
        private System.Windows.Forms.Button buttDivision;
        private System.Windows.Forms.Label labelRes;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button min_btn_cal;
        private System.Windows.Forms.Button max_btn_cal;
        private System.Windows.Forms.Button close_btn_cal;
    }
}