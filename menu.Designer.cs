﻿
namespace lab1
{
    partial class menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu));
            this.min_btn_login = new System.Windows.Forms.Button();
            this.max_btn_login = new System.Windows.Forms.Button();
            this.close_btn_login = new System.Windows.Forms.Button();
            this.lbl_menu = new System.Windows.Forms.Label();
            this.btn_calculator = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.pos_gusser_btn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // min_btn_login
            // 
            this.min_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.min_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.min_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("min_btn_login.BackgroundImage")));
            this.min_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.min_btn_login.FlatAppearance.BorderSize = 0;
            this.min_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.min_btn_login.Location = new System.Drawing.Point(673, 16);
            this.min_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.min_btn_login.Name = "min_btn_login";
            this.min_btn_login.Size = new System.Drawing.Size(24, 26);
            this.min_btn_login.TabIndex = 16;
            this.min_btn_login.UseVisualStyleBackColor = false;
            this.min_btn_login.Click += new System.EventHandler(this.min_btn_login_Click);
            // 
            // max_btn_login
            // 
            this.max_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.max_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.max_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("max_btn_login.BackgroundImage")));
            this.max_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.max_btn_login.FlatAppearance.BorderSize = 0;
            this.max_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.max_btn_login.Location = new System.Drawing.Point(703, 14);
            this.max_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.max_btn_login.Name = "max_btn_login";
            this.max_btn_login.Size = new System.Drawing.Size(21, 28);
            this.max_btn_login.TabIndex = 17;
            this.max_btn_login.UseVisualStyleBackColor = false;
            this.max_btn_login.Click += new System.EventHandler(this.max_btn_login_Click);
            // 
            // close_btn_login
            // 
            this.close_btn_login.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.close_btn_login.BackColor = System.Drawing.Color.RosyBrown;
            this.close_btn_login.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("close_btn_login.BackgroundImage")));
            this.close_btn_login.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.close_btn_login.FlatAppearance.BorderSize = 0;
            this.close_btn_login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close_btn_login.Location = new System.Drawing.Point(730, 16);
            this.close_btn_login.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.close_btn_login.Name = "close_btn_login";
            this.close_btn_login.Size = new System.Drawing.Size(19, 23);
            this.close_btn_login.TabIndex = 18;
            this.close_btn_login.UseVisualStyleBackColor = false;
            this.close_btn_login.Click += new System.EventHandler(this.close_btn_login_Click);
            // 
            // lbl_menu
            // 
            this.lbl_menu.AutoSize = true;
            this.lbl_menu.Font = new System.Drawing.Font("Modern No. 20", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_menu.Location = new System.Drawing.Point(320, 56);
            this.lbl_menu.Name = "lbl_menu";
            this.lbl_menu.Size = new System.Drawing.Size(140, 41);
            this.lbl_menu.TabIndex = 19;
            this.lbl_menu.Text = "MENU";
            this.lbl_menu.Click += new System.EventHandler(this.label1_Click);
            // 
            // btn_calculator
            // 
            this.btn_calculator.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_calculator.ForeColor = System.Drawing.Color.RosyBrown;
            this.btn_calculator.Location = new System.Drawing.Point(218, 142);
            this.btn_calculator.Name = "btn_calculator";
            this.btn_calculator.Size = new System.Drawing.Size(379, 39);
            this.btn_calculator.TabIndex = 21;
            this.btn_calculator.Text = "Calculator";
            this.btn_calculator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_calculator.UseVisualStyleBackColor = true;
            this.btn_calculator.Click += new System.EventHandler(this.btn_calculator_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.min_btn_login);
            this.panel1.Controls.Add(this.max_btn_login);
            this.panel1.Controls.Add(this.close_btn_login);
            this.panel1.Location = new System.Drawing.Point(-5, -5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(781, 58);
            this.panel1.TabIndex = 22;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.RosyBrown;
            this.button1.Location = new System.Drawing.Point(218, 202);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(379, 45);
            this.button1.TabIndex = 23;
            this.button1.Text = "Decryption \\ Encryption";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pos_gusser_btn
            // 
            this.pos_gusser_btn.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pos_gusser_btn.ForeColor = System.Drawing.Color.RosyBrown;
            this.pos_gusser_btn.Location = new System.Drawing.Point(218, 266);
            this.pos_gusser_btn.Name = "pos_gusser_btn";
            this.pos_gusser_btn.Size = new System.Drawing.Size(379, 39);
            this.pos_gusser_btn.TabIndex = 24;
            this.pos_gusser_btn.Text = "Position Guesser";
            this.pos_gusser_btn.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.pos_gusser_btn.UseVisualStyleBackColor = true;
            this.pos_gusser_btn.Click += new System.EventHandler(this.pos_gusser_btn_Click);
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RosyBrown;
            this.ClientSize = new System.Drawing.Size(776, 476);
            this.Controls.Add(this.pos_gusser_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_calculator);
            this.Controls.Add(this.lbl_menu);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "menu";
            this.Text = "menucs";
            this.Load += new System.EventHandler(this.menu_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button min_btn_login;
        private System.Windows.Forms.Button max_btn_login;
        private System.Windows.Forms.Button close_btn_login;
        private System.Windows.Forms.Label lbl_menu;
        private System.Windows.Forms.Button btn_calculator;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button pos_gusser_btn;
    }
}