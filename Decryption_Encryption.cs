﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace lab1
{
    public partial class Decryption_Encryption : Form
    {
        public Decryption_Encryption()
        {
            InitializeComponent();
            textBox_alphabet.Text = "abcdefghijklmnoprstuvyz";

        }

        private void Run()
        {
            if (radiobtn_CeaserCipher.Checked && textBox_key.Text != "")
            {
                if (radioBtn_Enc.Checked && textBox_key.Text != "") 
                {
                    try
                    {
                        lbl_result.Text = En_Decryption.Ceasar(textBox_alphabet.Text, textBox_message.Text, int.Parse(textBox_key.Text));
                    }
                    catch
                    {
                        MessageBox.Show("Rot must be integer!");
                    }
                }
                else if (radioBtn_Decry.Checked && textBox_key.Text != "")
                {
                    try
                    {
                        lbl_result.Text = En_Decryption.Ceasar(textBox_alphabet.Text, textBox_message.Text, -int.Parse(textBox_key.Text));
                    }
                    catch{
                        MessageBox.Show("Rot must be integer!");
                    }
                   
                }
            }
            else if (radio_btn_Vigenèrecipher.Checked && textBox_key.Text != "")
            {
                if (radioBtn_Enc.Checked && textBox_key.Text != "")
                {
                    lbl_result.Text = En_Decryption.Vigenère(textBox_alphabet.Text, textBox_message.Text, textBox_key.Text, true);
                }
                else if (radioBtn_Decry.Checked && textBox_key.Text != "")
                {
                    lbl_result.Text = En_Decryption.Vigenère(textBox_alphabet.Text, textBox_message.Text, textBox_key.Text, false);
                }
            }
            else //error
            {
            }
        }

        private void textBox_alphabet_TextChanged(object sender, EventArgs e)
        {
            Run();
        }

        private void radioBtn_Decry_CheckedChanged(object sender, EventArgs e)
        {
            Run();
        }

        private void Decryption_Encryption_Load(object sender, EventArgs e)
        {

        }

        private void textBox_key_TextChanged_1(object sender, EventArgs e)
        {
            Run();
        }

        private void textBox_message_TextChanged_1(object sender, EventArgs e)
        {
            Run();
        }

        private void radioBtn_Enc_CheckedChanged_1(object sender, EventArgs e)
        {
            Run();
        }

        private void radiobtn_CeaserCipher_CheckedChanged_1(object sender, EventArgs e)
        {
            Run();
        }

        private void radio_btn_Vigenèrecipher_CheckedChanged_1(object sender, EventArgs e)
        {
            Run();
        }
    }
}
