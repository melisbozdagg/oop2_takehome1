﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class Position_Guesser : Form
    {
        pos_guesser PGclass;

        public Position_Guesser()
        {
            InitializeComponent();
        }

        private void Position_Guesser_Load(object sender, EventArgs e)
        {
            PGclass = new pos_guesser();
            label_results.Text = PGclass.coordinates();
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            PGclass.reset();
            label_results.Text = "0x0" + Environment.NewLine;
        }

        private void btn_nw_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.NW);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_n_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.N);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_ne_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.NE);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_w_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.W);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_e_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.E);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_sw_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.SW);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_s_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.S);
            label_results.Text += PGclass.coordinates();
        }

        private void btn_se_Click(object sender, EventArgs e)
        {
            PGclass.Go(pos.SE);
            label_results.Text += PGclass.coordinates();
        }

       
    }
}
