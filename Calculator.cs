﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{



    public partial class Calculator : Form
    {

        public class Calc
        {

            public static double Plus(double num1, double num2)
            {
                return num1 + num2;
            }
            public static double Minus(double num1, double num2)
            {
                return num1 - num2;
            }
            public static double Multip(double num1, double num2)
            {
                return num1 * num2;
            }
            public static double Divison(double num1, double num2)
            {
                return num1 / num2;
            }

        }

        public Calculator()
        {
            InitializeComponent();
        }


        Button OldClick;
        private void buttPlus_Click(object sender, EventArgs e)
        {
            double result = 0;
            bool check = false;
            if (txtInput1.Text == "" || txtInput2.Text == "")
            {
                labelRes.Text = "PLEASE FILL THE EMPTY SPACES";
                check = true;
            }
            else
            {
                bool isNumerical1 = int.TryParse(txtInput1.Text, out _);
                bool isNumerical2 = int.TryParse(txtInput2.Text, out _);
                if (isNumerical1 && isNumerical2)
                {
                    result = Calculator.Calc.Plus(Convert.ToDouble(txtInput1.Text), Convert.ToDouble(txtInput2.Text));
                    labelRes.Text = txtInput1.Text + " + " + txtInput2.Text + " = " + result.ToString();

                }
                else
                {
                    labelRes.Text = "   -ERROR-  ENTER VALID NUMBER";
                    check = true;
                }
            }

            BackgroundColorChange(result, check);
            txtInput1.Text = "";
            txtInput2.Text = "";
            ButtonOldColor((Button)sender);


        }

        private void buttMinus_Click(object sender, EventArgs e)
        {
            double result = 0;
            bool check = false;
            if (txtInput1.Text == "" || txtInput2.Text == "")
            {
                labelRes.Text = "PLEASE FILL THE EMPTY SPACES";
                check = true;
            }
            else
            {
                bool isNumerical1 = int.TryParse(txtInput1.Text, out _);
                bool isNumerical2 = int.TryParse(txtInput2.Text, out _);
                if (isNumerical1 && isNumerical2)
                {
                    result = Calculator.Calc.Minus(Convert.ToDouble(txtInput1.Text), Convert.ToDouble(txtInput2.Text));
                    labelRes.Text = txtInput1.Text + " - " + txtInput2.Text + " = " + result.ToString();

                }
                else
                {
                    labelRes.Text = "-ERROR-";
                    check = true;
                }
            }


            BackgroundColorChange(result, check);
            txtInput1.Text = "";
            txtInput2.Text = "";
            ButtonOldColor((Button)sender);
        }

        private void buttMultip_Click(object sender, EventArgs e)
        {
            double result = 0;
            bool check = false;
            if (txtInput1.Text == "" || txtInput2.Text == "")
            {
                labelRes.Text = "PLEASE FILL THE EMPTY SPACES";
                check = true;
            }
            else
            {
                bool isNumerical1 = int.TryParse(txtInput1.Text, out _);
                bool isNumerical2 = int.TryParse(txtInput2.Text, out _);
                if (isNumerical1 && isNumerical2)
                {
                    result = Calculator.Calc.Multip(Convert.ToDouble(txtInput1.Text), Convert.ToDouble(txtInput2.Text));
                    labelRes.Text = txtInput1.Text + " * " + txtInput2.Text + " = " + result.ToString();


                }
                else
                {
                    labelRes.Text = "-ERROR-";
                    check = true;
                }
            }
            BackgroundColorChange(result, check);
            txtInput1.Text = "";
            txtInput2.Text = "";
            ButtonOldColor((Button)sender);

        }

        private void buttDivision_Click(object sender, EventArgs e)
        {
            double result = 0;
            bool check = false;

            if (txtInput1.Text == "" || txtInput2.Text == "")
            {
                labelRes.Text = "PLEASE FILL THE EMPTY SPACES";
                check = true;

            }
            else if (txtInput2.Text == "0")
            {
                labelRes.Text = "-ERROR-";
                check = true;
            }
            else
            {
                bool isNumerical1 = int.TryParse(txtInput1.Text, out _);
                bool isNumerical2 = int.TryParse(txtInput2.Text, out _);
                if (isNumerical1 && isNumerical2)
                {
                    result = Calculator.Calc.Divison(Convert.ToDouble(txtInput1.Text), Convert.ToDouble(txtInput2.Text));
                    labelRes.Text = txtInput1.Text + " / " + txtInput2.Text + " = " + result.ToString();

                }
                else
                {
                    labelRes.Text = "-ERROR-";
                }

            }
            BackgroundColorChange(result, check);
            txtInput1.Text = "";
            txtInput2.Text = "";
            ButtonOldColor((Button)sender);
        }

        private void BackgroundColorChange(double number, bool check)
        {
            if (check == true)
            {
                labelRes.BackColor = SystemColors.Control;
            }
            else if (number > 0)
            {
                labelRes.BackColor = Color.Green;
            }
            else
            {
                labelRes.BackColor = Color.Red;

            }
        }

        private void buttMultip_MouseHover(object sender, EventArgs e)
        {
            Old = ((Button)sender).ForeColor;
            ((Button)sender).ForeColor = Color.Red;

        }
        Color Old;
        private void buttMultip_MouseLeave(object sender, EventArgs e)
        {
            ((Button)sender).ForeColor = Old;
        }
        private void ButtonOldColor(Button btn)
        {
            if (OldClick != null)
                OldClick.ForeColor = Color.Black;
            OldClick = btn;
            Old = btn.ForeColor = Color.Blue;

        }

        private void min_btn_cal_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private int clickCounter_cal = 0;
        private void max_btn_cal_Click(object sender, EventArgs e)
        {
            if (clickCounter_cal % 2 == 0)
            {
                this.WindowState = FormWindowState.Maximized;
                clickCounter_cal++;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                clickCounter_cal++;
            }
        }

        private void close_btn_cal_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        int mouse_x = 0;
        int mouse_y = 0;
        bool mouse_down;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouse_down)
            {
                mouse_x = MousePosition.X - 200;
                mouse_y = MousePosition.Y - 40;
                this.SetDesktopLocation(mouse_x, mouse_y);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_down = true;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouse_down = false;
        }

        private void Calculator_Load(object sender, EventArgs e)
        {

        }
    }
}
