﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using static lab1.Functions;

namespace lab1
{
    public partial class SignUp : Form
    {
        User user = User.Instance;
        public SignUp()
        {
            InitializeComponent();
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }

        private void checkBox_robot_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BtnCreate_Click(object sender, EventArgs e)
        {
            if (tbox_newUsername.Text.Equals("") || tbox_newpass.Text.Equals("") || tbox_email.Text.Equals("") || tbox_newpass_R.Text.Equals(""))
            {
                MessageBox.Show("Please enter a value for all required fields.", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (checkBox_robot.Checked == false)
            {
                MessageBox.Show("Please verify again", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;

            }
            if (!tbox_newpass.Text.Equals(tbox_newpass_R.Text))
            {
                MessageBox.Show("Passwords are not equal!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }


            MessageBox.Show("Account Creation Successful!", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);

            user.createUser(tbox_newUsername.Text, MD5Hash(tbox_newpass.Text));


        }

        private void min_btn_signup_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;


        }

        private void max_btn_signup_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void close_btn_signup_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        int mouse_x = 0;
        int mouse_y = 0;
        bool mouse_down;

        private void panel_signup_MouseUp(object sender, MouseEventArgs e)
        {
            mouse_down = false;
        }

        private void panel_signup_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouse_down)
            {
                mouse_x = MousePosition.X - 200;
                mouse_y = MousePosition.Y - 40;
                this.SetDesktopLocation(mouse_x, mouse_y);
            }

        }

        private void panel_signup_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_down = true;
        }

    }
}